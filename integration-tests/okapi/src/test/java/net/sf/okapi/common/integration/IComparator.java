package net.sf.okapi.common.integration;

public interface IComparator<T> {
	boolean compare(T actual, T expected);
}
