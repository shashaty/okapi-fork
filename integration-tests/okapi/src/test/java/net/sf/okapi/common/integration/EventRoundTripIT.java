/**
 * 
 */
package net.sf.okapi.common.integration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.resource.RawDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author jimh
 *
 */
public class EventRoundTripIT extends BaseRoundTripIT {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * @param configId
	 * @param dirName
	 * @param extensions
	 */
	public EventRoundTripIT(final String configId, final String dirName, final List<String> extensions) {
		super(configId, dirName, extensions);
	}

	/**
	 * @param configId
	 * @param dirName
	 * @param extensions
	 * @param defaultTargetLocale
	 */
	public EventRoundTripIT(final String configId, final String dirName, final List<String> extensions,
			final LocaleId defaultTargetLocale) {
		super(configId, dirName, extensions, defaultTargetLocale);
	}

	@Override
	public void runTest(final boolean detectLocales, final File file, File subDir, final String customConfigPath, final IComparator comparator) {
		final String f = file.getName();
		final String root = file.getParent() + File.separator;
		final String xliff = root + f + xliffExtractedExtension;
		final String original = root + f;
		final String tkitMerged = root + f + ".tkitMerged";
		LocaleId source = LocaleId.ENGLISH;
		LocaleId target = defaultTargetLocale;
		if (detectLocales) {
			final List<String> locales = FileUtil.guessLanguages(file.getAbsolutePath());
			if (locales.size() >= 1) {
				source = LocaleId.fromString(locales.get(0));
			}
			if (locales.size() >= 2) {
				target = LocaleId.fromString(locales.get(1));
			}
		}

		try {
			logger.info(f);
			FilterConfigurationMapper mapper = RoundTripUtils.extract(source, target, original, xliff, configId,
					customConfigPath);
			RoundTripUtils.merge(source, target, original, xliff, tkitMerged, configId, customConfigPath, mapper);
			IParameters params = mapper.getParameters(mapper.getConfiguration(configId));

			try (RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", source, target);
					RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", source, target)) {
				filter.setFilterConfigurationMapper(mapper);
				final List<Event> o = IntegrationtestUtils.getEvents(filter, ord, params);
				final List<Event> t = IntegrationtestUtils.getEvents(filter, trd, params);
				assertTrue("Compare Lines: " + f, comparator.compare(o, t));
			}
		} catch (final Throwable e) {
			if (!knownFailingFiles.contains(f)) {
				errCol.addError(new OkapiTestException(f, e));
				logger.error("Failing test: {}\n{}", f, e.getMessage());
			} else {
				logger.info("Ignored known failing file: {}", f);
			}
		}

	}
}
