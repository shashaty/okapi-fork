/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ParametersString;

import java.util.Iterator;

final class ParametersStringWorksheetConfigurationsOutput implements WorksheetConfigurations.Output<ParametersString> {
    @Override
    public ParametersString writtenWith(final Iterator<WorksheetConfiguration> worksheetConfigurationsIterator) {
        final ParametersString ps = new ParametersString();
        int number = 0;
        while (worksheetConfigurationsIterator.hasNext()) {
            ps.setGroup(
                String.valueOf(number),
                worksheetConfigurationsIterator.next().writtenTo(new ParametersStringWorksheetConfigurationOutput())
            );
            number++;
        }
        if (0 != number) {
            ps.setInteger(ParametersStringWorksheetConfigurationsInput.NUMBER, number);
        }
        final ParametersString root = new ParametersString();
        root.setGroup(ParametersStringWorksheetConfigurationsInput.NAME, ps);
        return root;
    }
}
