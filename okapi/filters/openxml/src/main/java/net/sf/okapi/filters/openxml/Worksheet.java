/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

interface Worksheet {
	void readWith(final XMLEventReader reader) throws XMLStreamException;
	void writeWith(final XMLEventWriter writer) throws XMLStreamException;

	class Default implements Worksheet {
		private static final String SHEET_VIEW = "sheetView";
		private static final String CELL = "c";
		private static final String VALUE = "v";
		private static final String INLINE_STRING = "is";
		private static final QName CELL_LOCATION_REFERENCE = new QName("r");
		private static final QName CELL_TYPE = new QName("t");
		private static final String CELL_TYPE_INLINE_STRING = "inlineStr";
		private static final String CELL_TYPE_SHARED_STRING = "s";
		private static final QName CELL_STYLE = new QName("s");
		private static final int FAKE_INDEX = -1;
		private static final Set<Character> EXTRA_WHITESPACES = new HashSet<>(Arrays.asList('\t', '\r', '\n'));
		private final ConditionalParameters conditionalParameters;
		private final XMLEventFactory eventFactory;
		private final SharedStrings sharedStrings;
		private final ExcelStyles styles;
		private final String name;
		private final WorksheetFragments fragments;
		private Set<Integer> excludedRows;
		private Set<String> excludedColumns;
		private Set<Integer> metadataRows;
		private Set<String> metadataColumns;
		private CellReferencesRange cellReferencesRange;
		private MarkupBuilder markupBuilder;
		private ListIterator<XMLEvent> iterator;

		Default(
			final ConditionalParameters conditionalParameters,
			final XMLEventFactory eventFactory,
			final SharedStrings sharedStrings,
			final ExcelStyles styles,
			final String name,
			final WorksheetFragments fragments
		) {
			this.conditionalParameters = conditionalParameters;
			this.eventFactory = eventFactory;
			this.sharedStrings = sharedStrings;
			this.styles = styles;
			this.name = name;
			this.fragments = fragments;
		}

		@Override
		public void readWith(final XMLEventReader reader) throws XMLStreamException {
			this.fragments.readWith(reader);
			this.excludedRows = new HashSet<>(
				this.conditionalParameters.worksheetConfigurations().excludedRowsFor(this.name)
			);
			this.excludedRows.addAll(this.fragments.hiddenRows());
			this.metadataRows = this.conditionalParameters.worksheetConfigurations().metadataRowsFor(this.name);
			this.excludedRows.addAll(this.metadataRows);
			this.excludedColumns = new HashSet<>(
				this.conditionalParameters.worksheetConfigurations().excludedColumnsFor(this.name)
			);
			this.excludedColumns.addAll(this.fragments.hiddenColumns());
			this.metadataColumns = this.conditionalParameters.worksheetConfigurations().metadataColumnsFor(this.name);
			this.excludedColumns.addAll(this.metadataColumns);
			this.markupBuilder = new MarkupBuilder(new Markup.General(new ArrayList<>()));
			this.iterator = this.fragments.events().listIterator();

			while (iterator.hasNext()) {
				final XMLEvent e = iterator.next();
				if (e.isStartElement()) {
					final StartElement se = e.asStartElement();
					if (SHEET_VIEW.equals(se.getName().getLocalPart())) {
						this.markupBuilder.add(new MarkupComponent.Start(this.eventFactory, se));
						continue;
					} else if (CELL.equals(se.getName().getLocalPart())) {
						readCell(se);
						continue;
					}
				} else if (e.isEndElement()) {
					final EndElement ee = e.asEndElement();
					if (SHEET_VIEW.equals(ee.getName().getLocalPart())) {
						this.markupBuilder.add(new MarkupComponent.End(ee));
						continue;
					}
				}
				this.markupBuilder.add(e);
			}
		}

		private void readCell(final StartElement startElement) {
			final Attribute typeAttr = startElement.getAttributeByName(CELL_TYPE);
			if (typeAttr != null
				&& (CELL_TYPE_INLINE_STRING.equals(typeAttr.getValue()) || CELL_TYPE_SHARED_STRING.equals(typeAttr.getValue()))) {
				if (CELL_TYPE_INLINE_STRING.equals(typeAttr.getValue())) {
					this.markupBuilder.add(newCellStartElement(startElement, typeAttr));
				} else {
					this.markupBuilder.add(startElement);
				}
				this.cellReferencesRange = cellReferencesRange(
					new CellReference(startElement.getAttributeByName(CELL_LOCATION_REFERENCE).getValue())
				);
				final boolean excluded = !this.cellReferencesRange.partialMatch(this.excludedRows, this.excludedColumns)
					|| styleExcluded(startElement);
				while (this.iterator.hasNext()) {
					final XMLEvent e = this.iterator.next();
					if (e.isEndElement() && e.asEndElement().getName().equals(startElement.getName())) {
						this.markupBuilder.add(e);
						break;
					}
					if (!e.isStartElement()) {
						this.markupBuilder.add(e);
						continue;
					}
					final StartElement se = e.asStartElement();
					if (VALUE.equals(se.getName().getLocalPart())) {
						readValue(se, excluded);
					} else if (INLINE_STRING.equals(se.getName().getLocalPart())) {
						readInlineString(se, excluded);
					} else {
						this.markupBuilder.add(e);
					}
				}
			} else {
				this.markupBuilder.addAll(currentAndSiblingEventsOf(startElement));
			}
		}

		private StartElement newCellStartElement(final StartElement startElement, final Attribute attribute) {
			final List<Attribute> newAttributes = new ArrayList<>();
			final Iterator iterator = startElement.getAttributes();
			while (iterator.hasNext()) {
				final Attribute a = (Attribute) iterator.next();
				if (CELL_TYPE.equals(a.getName())) {
					newAttributes.add(
						this.eventFactory.createAttribute(attribute.getName(), CELL_TYPE_SHARED_STRING)
					);
					continue;
				}
				newAttributes.add(a);
			}
			return this.eventFactory.createStartElement(
				startElement.getName(),
				newAttributes.iterator(),
				startElement.getNamespaces()
			);
		}

		private CellReferencesRange cellReferencesRange(final CellReference cellReference) {
			return this.fragments.cellReferencesRanges().stream()
				.filter(r -> r.first().equals(cellReference))
				.findFirst()
				.orElse(new CellReferencesRange(cellReference));
		}

		private boolean styleExcluded(final StartElement startElement) {
			boolean excluded = false;
			final Attribute styleAttr = startElement.getAttributeByName(CELL_STYLE);
			if (styleAttr != null) {
				final int styleIndex = Integer.parseUnsignedInt(styleAttr.getValue());
				final ExcelStyles.CellStyle style = styles.getCellStyle(styleIndex);
				// I'm going to start with a naive implementation that should
				// basically be fine, but not ideal if we're excluding large numbers
				// of colors.
				for (final String excludedColor : this.conditionalParameters.tsExcelExcludedColors) {
					if (style.fill.matchesColor(excludedColor)) {
						excluded = true;
						break;
					}
				}
			}
			return excluded;
		}

		private void readValue(final StartElement startElement, final boolean excluded) {
			this.markupBuilder.add(startElement);
			while (this.iterator.hasNext()) {
				final XMLEvent e = this.iterator.next();
				if (e.isEndElement() && e.asEndElement().getName().equals(startElement.getName())) {
					this.markupBuilder.add(e);
					break;
				}
				if (!e.isCharacters()) {
					this.markupBuilder.add(e);
					continue;
				}
				final String s = e.asCharacters().getData().trim();
				if (s.isEmpty() || s.chars().mapToObj(c -> (char) c).allMatch(c -> EXTRA_WHITESPACES.contains(c))) {
					this.markupBuilder.add(e);
					continue;
				}
				final int origIndex = sharedStringIndex(s);
				final int newIndex = this.sharedStrings.nextIndex();
				this.sharedStrings.add(
					new SharedStrings.Item(
						origIndex,
						newIndex,
						Collections.emptyList(),
						this.name,
						this.cellReferencesRange,
						excluded,
						metadataContext()
					)
				);
				// Replace the event with one that contains the new index
				this.markupBuilder.add(eventFactory.createCharacters(String.valueOf(newIndex)));
			}
		}

		private static int sharedStringIndex(final String value) {
			try {
				return Integer.parseUnsignedInt(value);
			} catch (NumberFormatException e) {
				throw new IllegalStateException("Unexpected shared string index '" + value + "'");
			}
		}

		private void readInlineString(final StartElement startElement, final boolean excluded) {
			final List<XMLEvent> events = new ArrayList<>();
			while (this.iterator.hasNext()) {
				final XMLEvent e = this.iterator.next();
				if (e.isEndElement() && e.asEndElement().getName().equals(startElement.getName())) {
					break;
				}
				events.add(e);
			}
			final int newIndex = this.sharedStrings.nextIndex();
			this.sharedStrings.add(
				new SharedStrings.Item(
					FAKE_INDEX,
					newIndex,
					events,
					this.name,
					this.cellReferencesRange,
					excluded,
					metadataContext()
				)
			);
			this.markupBuilder.add(
				this.eventFactory.createStartElement(
					startElement.getName().getPrefix(),
					startElement.getName().getNamespaceURI(),
					VALUE
				)
			);
			this.markupBuilder.add(
				this.eventFactory.createCharacters(String.valueOf(newIndex))
			);
			this.markupBuilder.add(
				this.eventFactory.createEndElement(
					startElement.getName().getPrefix(),
					startElement.getName().getNamespaceURI(),
					VALUE
				)
			);
		}

		private List<XMLEvent> currentAndSiblingEventsOf(final StartElement startElement) {
			final List<XMLEvent> events = new ArrayList<>();
			events.add(startElement);
			while (this.iterator.hasNext()) {
				final XMLEvent e = this.iterator.next();
				events.add(e);
				if (e.isEndElement() && e.asEndElement().getName().equals(startElement.getName())) {
					break;
				}
			}
			return events;
		}

		private MetadataContext metadataContext() {
			final boolean metadataRow = this.cellReferencesRange.rows().stream()
				.anyMatch(r -> this.metadataRows.contains(r));
			final boolean metadataColumn = this.cellReferencesRange.columns().stream()
				.anyMatch(c -> this.metadataColumns.contains(c));
			final MetadataContext context;
			if (metadataRow && metadataColumn) {
				context = MetadataContext.ROW_AND_COLUMN;
			} else if (metadataRow) {
				context = MetadataContext.ROW;
			} else if (metadataColumn) {
				context = MetadataContext.COLUMN;
			} else {
				context = MetadataContext.NONE;
			}
			return context;
		}

		@Override
		public void writeWith(final XMLEventWriter writer) throws XMLStreamException {
			for (final XMLEvent event : this.markupBuilder.build().getEvents()) {
				writer.add(event);
			}
		}
	}
}
