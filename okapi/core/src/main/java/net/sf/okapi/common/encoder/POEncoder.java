/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.encoder;

import java.nio.charset.CharsetEncoder;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.resource.Property;

/**
 * Implements {@link IEncoder} for PO file format.
 *
 * Encodes characters following C escape sequences.
 * See https://en.wikipedia.org/wiki/Escape_sequences_in_C
 */
public class POEncoder implements IEncoder {

    @Override
    public void setOptions(IParameters params, String encoding, String lineBreak) {
        //this.lineBreak = lineBreak;
    }

    @Override
    public void reset() {
    }

    @Override
    public String encode(String text, EncoderContext context) {
        if (context != EncoderContext.TEXT)
            return text;
        StringBuilder builder = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); i++) {
            final char ch = text.charAt(i);
            builder.append(encode(ch, context));
        }
        return builder.toString();
    }

    @Override
    public String encode(char value, EncoderContext context) {
        if (context != EncoderContext.TEXT)
            return String.valueOf(value);
        switch (value) {
            case '\\':
                // escape backslash
                return "\\\\";
            case '"':
                // escape quotes
                return "\\\"";
// TODO: Is not clear if the apostrophe must be encoded
//            case '\'':
//                // escape apostrophe
//                return "\\'";
            case '\t':
                // escape tab characters
                return "\\t";
            case '\n':
                // escape newlines
                return "\\n";
            case '\r':
                // escape carriage returns
                return "\\r";
            case '\f':
                // escape form feeds
                return "\\f";
            case '\u000B':
                // escape vertical tab
                return "\\v";
            case '\b':
                // escape backspace
                return "\\b";
            case '\u0007':
                // escape alarm bell
                return "\\a";
            default:
                return String.valueOf(value);
        }
    }

    @Override
    public String encode(int value, EncoderContext context) {
        if (Character.isSupplementaryCodePoint(value)) {
            return new String(Character.toChars(value));
        }
        return String.valueOf((char) value);
    }

    @Override
    public String toNative(String propertyName, String value) {
        if (Property.APPROVED.equals(propertyName)) {
            if ((value != null) && (value.equals("no"))) {
                return "fuzzy";
            } else { // Don't set the fuzzy flag
                return "";
            }
        }

        // No changes for the other values
        return value;
    }


    @Override
    public String getLineBreak() {
        return "\n";
    }

    @Override
    public CharsetEncoder getCharsetEncoder() {
        return null;
    }

    @Override
    public IParameters getParameters() {
        return null;
    }

    @Override
    public String getEncoding() {
        return "";
    }

}
